# README #

Preprocessing and analyzing the flowCytometry data provided by Dr. Paul Wallace, which includes 100 patients and 100 healthy.

* Everything up to this date is tested on R 3.0.1 and ubuntu 12. Following packages should be installed:

1. `flowCore`


```
#!r

source("http://bioconductor.org/biocLite.R")
biocLite("flowCore")
Install flowMatch (via bioconductor)
Install flowPeaks (via bioconductor)
```

2. `RcppArmadillo`

3. The latest version of `flowMatch` in Bioconductor does not have the dynamic template so it should be updated

4. `e1071`
5. `fpc`
6. `vegan`
7. `vegan3d`

### Preprocess the data###
In preprocess folder, run:

1. `setup-preprocess.R`

2. `preprocess.R`

	* `reading.R`
	* `unmixing_methods.R`
	* `transformation.R`
	* `which_k.R`

it will take care of cleaning up the data, transformation and spectral
unmixing; you can chose between logbias or asinh
transformation; the default is on logbias transformation

### Stat over the number of samples and subtypes ###
We should first obtain which subtypes of AML and number of patients
per subtype is available based on the current data:

1. `cd sample-script/`

2. `./extract-samples.sh`. Its input file is samples-reading.csv (it is "Tab" separated.) and its outputs are:
   samples-ok (list of samples with known AML subtype), samples-partial (list of samples that need manual review) and samples-nodisease (list of samples which has no disease.).

3. `./count-disease.sh` to get the list of the subtypes and number of patients per subtype.

4. `./which-samples.sh` input to it is the subtype of interest and output is the list of samples per that subtype.

5. In `interface.R`, samplesPerSubtype function run the script from R. Just make sure you have `which-samples.sh` in `../AML-subtype/sample-script/`

### Running aml-vs-healthy experiment

1. `aml_classification.R`

2. `cross_validation.R`

3. `specific.R`

### Running the subtype-related experiment


1. `setup.R`

   Choose "fsubtype" when is prompted (so `init.R` be run, you can also change the alpha in init.R, it is responsible for weight of each
   term in scoring function which assess the similarity of sample to
   template)

   Choose "no" when prompted for question "is this a test experiment"
   ("yes" if you want to run it just over one tube)

2. `subtype_vs_all.R`

	Cross validation over samples of subtype vs ~subtype (Meeting July 2th as Bartek suggested)
	it takes 5 hours on bergen to run LOO over all samples
	on all tubes (`alpha <- 0.5`)

	Output of this code is saved in: `../AML-subtype/subtype-output/`

	The output of this function is the scores of similarity of samples
    to two templates which are saved in rds format in `.../AML-subtype/subtype-output/score-0.5/`

	Note: the old output for different alpha is saved in `input/subtype/score-matrix-<alpha>.rds`

3. `fv_score.R`

	apply svm over vector representing sample's
	scores across tubes, it takes 1 hour on bergen

	* The created feature vectors are saved at
    `.../AML-subtype/subtype-output/single-score-0.5/`
	(the last folder can be `single-score-0.5` or `score-diff-0.5`
    depending on the executed experiment)

	* The final results of svm are saved in
      `../AML-subtype/subtype-output/single-score-0.5/<subtype name>`
	  for example for subtype Monocytic differentiation
	  `AML-subtype/subtype-output/single-score-0.5/MONOCYTIC-DIFFERENTIATION`

	* The original implementation of `fv_score.R` is
      [here](https://bitbucket.org/baharak/aml-proc/diff/amlprocessing/AML-subtype/fv_score.R?diff1=13e505575c80&diff2=703c71f29563f91af3c4516bb106a1ceeeeec569&at=svm-fixed)
      where the parameters of svm are not fully optimized.

4. `svm-prob.R`
plot the classification of svm based on the computed probability for which class each sample belongs to

5. `svm-param.R`

	comparison between changes in final results over changing gamma and c

6. `parsing.py`

	read the specific mc files and for center of each mc; print the marker and expression level of it

7. `marker_expression.R`

   compute cumulative distribution function for
   AML and healthy marker expression & plot them:
	  The saved plots are in
	  `../amlprocessing/output/expression-output/`

	  They plots are manually read and transferred to final-interpret.csv:
	  for each tube and each marker, the orange line intersection with AML and  healthy cdf, which is the "with probability of 80%, the marker expression of marker x will be <= z"

	  TODO: for a fair comparison between level of expression across different markers, FS and SS needs to be normalized to be between 0-1.


8. `parse-part1.py`

 * Run parsing.py to compute the selected file in
 `../AML-subtype/subtype-output/MONOCYTIC-DIFFERENTIATION/`

* Run `parser-part1.py` on following files to compute `filtered-subtype.csv`

		read the marker expression level for specific mc
			../AML-subtype/subtype-output/MONOCYTIC-DIFFERENTIATION/selected

		read the expression level of markers in
			../amlprocessing/output/expression-output/final-interpret.csv


### Running the ISOMAP related experiment

`Ranking.R`

Computing the distance between the samples (AML-vs-healthy and
subclasses of AML) and represent them after applying ISOMAP (`../amlprocessing/ranking.R`)

* `AML-vs-healthy`:

	This part of the code does aml-vs-healthy analysis:

* For subtype:

	The first part of `fv_score.R` creates feature vectors for each subclass of AML (e.g, `vectors-of-MONOCYTIC-DIFFERENTIATION.rds`): out of those files; the ones which has the subclass are concatenated together and then we have applied Isomap on it


### Investigating the importance of initial clustering by different clustering methods in classification scores

`initial_cluster.R`

   Visualize and plot the clusters to demonstrate
   the difference between kmeans clustering and DBSCAN
   to achieve a better visualization, apply
   PCA on each flowframe and then cluster it

   The output is saved on my local machine at `../repo/amlprocessing/benchmark`

### To run the flowPeaks benchmark:

1. `setup.R`

   as prompted, type : "bp"


* The input is the transformed data (log-bias?) of raw data.
Now, based on the experiment type; chose one of the following:

	* for aml vs healthy:

	  in `setup.R`: set `aml.healthy.flowPeaks <- TRUE` and
	  `subtype.flowPeaks <-FALSE` (everything else should be also FALSE)
	  It takes 2:30 on Bergen
	  the output is saved at:
	  `.../amlprocessing//benchmark/flowPeak//output/mix-flowSets/aml-healthy/`

	* for running subclasses of AML:

     in `setup.R`: set `subtype.flowPeaks <- TRUE` and `aml.healthy.flowPeaks <-
     FALSE` (everything else should be also FALSE)
	 The output is saved at:

### Plotting the svm output:

* `/AML-subtype/svm-prob.R`
  Plot the probability that SVM computes based on the similarity of the
  sample to specific class
  Input :
		By setting `probability = TRUE`, SVM computes the
		probability of belonging a sample to each subclass.
		The saved probability is computed and stored as:
		data summarization with
		`FlowMatch:`
			set `probability=TRUE` in `svmProcessing()` (is a function in
		    `.../AML-subtype/core.R`) and the probability are saved in
            `.../AML-subtype/prob-{subtype name}.rds`
		`FlowPeaks:`
			set `probability=TRUE` in `svmProcessFP()` (is a function in
			`.../benchmarks/flowPeaks/interface.R`) and the probability
            are saved in
           `.../benchmark/flowPeak/output/mix-flowSets/subtype/prob-{subtype
                                                                    name}.rds`

  Output:
	  * AML-subtype using flowMatch for creating the template
	    is saved in `AML-subtype/subtype-output/*.pdf`
	  * for AML-subtype using flowPeaks for summarizing the data
	    is saved in `benchmark/flowPeak/output/mix-flowSets/subtype/`



# FlowPeaks gating
Just checkout this commit and run it 763d898; the result for one tube is in copy
flowpeaks-subtype-gating-763d898

* Step 1. Computing the average mahalanobis distance over all samples of
every subtype. To do that, run the 2c879d3 from "tomerge" branch.

* Step 2. Run `gate-parser.py` of 1f3db98 from branch "repropaper"

* Step 3. Step 2 gives a pdf file of plots which has the cell perentile
  over different Mahalanobis distance.

* Step 4. Run commit 11d1d67 from branch "repropaper" to run the
  gating experiment for different gate size.


* The output files from Step 1 are saved in
  `.../benchmark/output/flowPeaks/mdqc/F*`
  of branch repropaper of 1f3db98.

* The output files of Step 4 are saved in
  `.../benchmark/flowPeak/output/mdqc-f-val/gate*`
  of branch gatesize of 11d1d67.