# ==========================================================================================
# Draw an ellipse in two dimension for a bivariate normal distribution (for internal use only)
# ==========================================================================================

ellipse <- function(mu, sigma, alpha=.05, npoints=250, ...)
{
  es <- eigen(sigma)
  e1 <- es$vec%*%diag(sqrt(es$val))
  r1 <- sqrt(qchisq(1-alpha,2))
  theta <- seq(0,2*pi,len=npoints)
  v1 <- cbind(r1*cos(theta),r1*sin(theta))
  pts=t(mu-(e1%*%t(v1)))
  lines(pts, ...)

}

# ==========================================================================================
# Calculate axis limits for a bivariate contour plot (for internal use only)
# input: the list of objects of class 'Cluster', alpha for (1-alpha)*100% contour
# The length of an axis is computed by sqrt of eigen value multiplied by the qhi-square
# distribution
# this function returns limits (min and max) for each dimension
# ==========================================================================================

limitcalc = function(clust.list, alpha=.05)
{
  r1 <- sqrt(qchisq(1-alpha,1)) #
  dim = length(get.center(clust.list[[1]]))
  #merge all means and all variance
  means = matrix(0, nrow=0, ncol=dim)
  vars = matrix(0, nrow=0, ncol=dim)

  for(i in 1:length(clust.list))
  {
    means = rbind(means, get.center(clust.list[[i]]))
    vars = rbind(vars, diag(get.cov(clust.list[[i]])))
  }

  width = r1 * sqrt(apply(vars, 2, max))
  min.pos = apply(means, 2, min) - width
  max.pos = apply(means, 2, max) + width
  diff = max.pos - min.pos
  max.pos = max.pos + diff/10 # a little extra space
  min.pos = min.pos - diff/10
  minmax = rbind(min.pos, max.pos)
  return(minmax)
}

# ==========================================================================================
# this is an internal function used to plot cluster, meta-cluster or template
# draw Ellipse countour plot matrix for high dimensional clusters
# This will produce a matrix of plot similar to the functions splom or pairs or flowPlot
# Input:
#   clusters : a list of object of clusss 'Cluster' defined in possibly more than one dimensions
#   axis.labels : a vector of length equal to the dimension of the clsuters
#                 ith entry of the vector denotes the label of ith diemnsion
#                 if not supplied we use FL1, FL2 etc.
#   axis.limits : a 2xdim matrix, ith colum represent the limit of th eith dimension
#               : if NULL provided , estimate the limits by the limitcalc function
#   alpha: (1-alpha)*100% quantile is used for plotting
#   add: True/False denoting if the classed should be added to an exisitng plot
#   col: the color used to draw the ellipses, if not provided use foreground colors


plot.cluster.contours <- function(clusters,
                                  clusters.two,
                                  range.y,
                                  range.x,
                                  axis.labels=NULL,
                                  axis.limits=NULL,
                                  alpha=.05,
                                  col = par("fg"),
                                  healthy.data,
                                  aml.data,...) {
  if(!is(clusters, 'list'))
    stop("The input argument 'clusters' to function plot.cluster.contours must be a list of object 'Clusters'\n")
  len=length(clusters)
  if(len == 0) {
    stop("The list of of clusters passed for plotting is  empty\n")
  }
  dim.data = length(get.center(clusters[[1]]))
  if(dim.data < 2)
    stop("The data should be at least two dimensional\n")
  # repeat color if needed
  if(length(col) < length(clusters)) {
    col = rep(col, length(clusters))
  }


  if(is.null(axis.labels)) {
    if(!is.null(names(get.center(clusters[[1]])))) {
      axis.labels = names(get.center(clusters[[1]]))
    }
    else {
      axis.labels = paste('FL', 1:dim.data, sep='')
    }
  }

  if(length(axis.labels) != dim.data) {
    stop("The number of axis labels are not equal to the dimension of data\n")
  }
  if(!is.null(axis.limits) && is(axis.limits,'matrix'))  {
    if(nrow(axis.limits)==2 && ncol(axis.limits) == dim.data)
      limits = axis.limits
    else
      limits=limitcalc(clusters, alpha=alpha)
  } else {
    limits=limitcalc(clusters, alpha=alpha)
  }

#  op <- par(no.readonly = TRUE)
  ## par(mfrow=c(dim.data-1,dim.data-1));
  par(mfrow = c(length(range.y), length(range.x)))
  par(mar=c(.5,.5,.5,.5));
  par(oma=c(4,5,3,3))

  len.y <- length(range.y)
  len.x <- length(range.x)
  for (i in 2:len.y) {
      y <- range.y[i]
      for (j in 1:len.x) {
        x <- range.x[j]
        plot(0, type='n', xlim= c(0, 1), ylim = c(0, 1))
#             xaxp = c(-1, 12, 12))

        if (i == length(range.y)) mtext(axis.labels[x], side = 1, line = 2.5)
        if (j == 1) mtext(axis.labels[y], side = 2, line = 2.5)
        abline(h = healthy.data[i], col = "darkblue")
        abline(v = healthy.data[j + 2], col = "darkblue")
        abline(h = aml.data[i], col = "red")
        abline(v = aml.data[j + 2], col = "red")
        for (k in 1:len) {
          cl=clusters[[k]]
          mu=cl@center[c(x,y)]
          sigma=cl@cov[c(x,y),c(x,y)]
          if (x == 5) {
              mu <- runif(length(mu), 0.8, 0.8)
          } else {
              mu <- runif(length(mu), 0.2, 0.4)
          }
          ellipse(mu, sigma = sigma, alpha = 0.9,
                 level = 0.3, nts = 250, col="red",
                 add = TRUE, ...)

          par(new = TRUE)

          cl=clusters.two[[k]]
          print("the cl is")
          print(cl)
          mu=cl@center[c(x,y)]
          sigma=cl@cov[c(x,y),c(x,y)]
          if (x == 5) {
              mu <- runif(length(mu), 0.2, 0.4)
          } else {
              mu <- runif(length(mu), 0.4, 0.9)
          }
          ellipse(mu, sigma = sigma, alpha = 0.9,
                  level = 0.3, nts = 250,
                  col="blue",...)
      }
    }
  }
  par(op) # restore old parameter
}
