## first input: ./output/expression-output/final-interpret.csv
## second input:  ./AML-subtype/subtype-output/MONOCYTIC-DIFFERENTIATION/selected/
## exp: python parse-part1.py ../output/expression-output/final-interpret.csv subtype-output/MONOCYTIC-DIFFERENTIATION/selected.csv
## TODO: make it to run for all the subtypes
import sys
import io
from collections import defaultdict

if __name__=='__main__':

    def readCdf(f):
        ## read cumulative distribution fun
        ## ./output/expression-output/final-interpret.csv
        hashtable = {}
        f.readline() # get rid of header
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            word = line.split(',')
            if max(map(len, word)) == 0:
                break
            i = 2 # start of fs
            while i < len(word):
                aml_val, healthy_val = map(float, word[i + 1 : i + 3])
                almost_same = (abs(aml_val - healthy_val) <= 0.5)
                marker = word[i]
                okay = (not almost_same or marker in ("FS", "SS")) # True if want all
                if len(word[0]) != 0 and okay:
                    pair = (word[0], marker)
                    hashtable[pair] = healthy_val
                i = i + 3
        return hashtable


    def specificMC(f):
        ## parse specific metacluster file in
        ## ./AML-subtype/subtype-output/MONOCYTIC-DIFFERENTIATION/selected/
        ## these files have no header
        d = {}
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            word = line.split('\t')
            for w in word[1:]:
                tokens = w.strip('()').split(',')
                if len(tokens) == 2:
                    p = (word[0], tokens[0])
                    v = float(tokens[1])
                    d.setdefault(p, []).append(v)
        return d


    f = open(sys.argv[1])
    hashtable = readCdf(f)
    g = open(sys.argv[2])
    d = specificMC(g)


    d_res = {}
    for common_key in set(d.keys()) & set(hashtable.keys()):
        vs = d[common_key]
        v2 = hashtable[common_key]
        exps = [
            None if abs(diff) < 1 else (False if diff < 0 else True)
            for diff in map(lambda x: x - v2, vs)
        ]
        # setdefault returns default={} or old
        d_sub = d_res.setdefault(common_key[0], {})
        for exp in exps:
            d_sub.setdefault(common_key[1], []).append(exp)

    for tube, d_sub in d_res.iteritems():
        l_sub = []
        for marker in ("FS", "SS"):
            l_sub.append((marker, d_sub.pop(marker)))
        l_sub += list(d_sub.items())
        row_cnt = len(l_sub[0][1])  # len of exps
        for row in range(row_cnt):
            print tube,
            for marker, exps in l_sub:
                print ", %s%s" % (
                    marker,
                    ("" if exps[row] is None else ("+" if True else "-")),
                ), # don't go into new line
            print
