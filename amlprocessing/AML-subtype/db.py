import contextlib
import os.path
import sqlite3
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import cohen_kappa_score
from statsmodels.stats.inter_rater import cohens_kappa, to_table
import math
import csv
from sklearn.metrics import precision_recall_fscore_support
from tabulate import tabulate

@contextlib.contextmanager
def sqlite_connection():
    db_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "all-subtypes-probs.sqlite")
    with sqlite3.connect(db_path) as conn:
        yield conn
    print "closing conn to db"

def fun(col):
    with sqlite_connection() as db:
        pids, tss, cols = zip(*list(db.execute("SELECT pid, ts, " + col + " from result")))
        pr_fval = precision_recall_fscore_support(tss, cols, average='weighted')
        labels = list(set(cols))
        ar = confusion_matrix(tss, cols, labels=labels)
        labels = map(get_labels, labels)
        write_csv(ar, labels, col)
        print tabulate(ar, headers=labels,
                       tablefmt="latex")
        ck = cohens_kappa(confusion_matrix(tss, cols))
        write_csv([[ck.kappa_max, ck.std_kappa, ck.pvalue_two_sided]],
                  ["kappa", "ASE", "2-tail pval"],
                  col + "-kappa")
        table = [[ck.kappa_max, ck.std_kappa, ck.pvalue_two_sided]]
        print tabulate(table, headers=["kappa", "ASE", "2-tail pval"],
                       tablefmt="latex")
        return pr_fval


def get_labels(c):
    ## get all the components of
    ## sum of power of two
    if c==0:
        return "other"
    labels = []
    classes = ["md", "gd", "nm", "ns"]
    while c!=0:
        n = c & -c
        labels.append(classes[int(math.log(n, 2))])
        c -= n
    return "+".join(labels)

def write_csv(matrix, labels, exp):
    with open('subtype-output/multi-conf-matrix-' + exp, 'wb') as csvfile:
        conf_m = csv.writer(csvfile, delimiter =',')
        conf_m.writerow(labels)
        for row in matrix:
            conf_m.writerow(row)


if __name__=="__main__":
    fval_gs = fun("gs"), 2
    fval_ms = fun("ms"), 2
    # todo: adding a for loop to change
    # props.prop
    labels = ["other", "md", "gd", "nm", "ns"]
    write_csv([list(fval_gs), list(fval_ms)],
              ["precision", "recall", "fval"],
              "fval")
    table = [[list(fval_gs), list(fval_ms)]]
    print tabulate([list(fval_gs), list(fval_ms)],
                   headers=["precision", "recall", "fval"],
                   tablefmt="latex")
