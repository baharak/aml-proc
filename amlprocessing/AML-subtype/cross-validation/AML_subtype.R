# the function to load all of the necessary functions
funcSource <- function(source.c, fun.name ) {
  fun.path <- paste(source.c, fun.name, sep='')
  if(!exists(fun.name)) source(fun.path)
}

# where the source code(main, interface and core) are.
source.c <- '/home/bsaberid/Dropbox/research/flowCy/wallace-code/codes/'

# load the inteface and core parts of the codes 
funcSource(source.c, 'AML_subtype_core.R')
funcSource(source.c, 'AML_subtype_interface.R')
funcSource(source.c, 'AML_subtype_debug_test.R')

# run the srcipt from the bash and get the list of samples per aml subtype
subtype.indices <- samples.per.subtype(tube.list, requested.aml.type)
# TODO : make it to works for specific sample(e.g, relapsed)
# the subtype.indices that is passed to it should be changed so it just 
# extract those specific samples that we requested.

# create a matrix, which rows shows tube.names and 
# columns are cluster Samples for each subtype of AML
clusters.subtypes.all.tubes <- clusters.per.AML.subtype(subtype.indices,
                                                                tube.list)

# create a vector of samples specific for each subtype
subtype.len <- list()
for (i in 1:length(subtype.indices)){
   subtype <- subtype.indices[[i]]
   subtype.len <- c (subtype.len, length(subtype))
}

# create a matrix of zero with number of raws as number of tubes
# and number of columns as the total number of samples in all subtypes
unlist.subtype.len <- unlist(subtype.len)
num.cols <- sum(unlist.subtype.len[1:length(unlist.subtype.len)])
num.raws <- length(tube.list)
#cross.validation.scores <- matrix(0, nrow=num.raws, ncol=num.cols)
cross.validation.scores <- matrix(0, nrow=num.raws, ncol=16)

# apply the cross validation over the clusters.subtypes.all.tubes
num.tubes <- length(tube.list)
for(tube in 1:1) { 
  cat('Tube', tube, ': ')
  scores.tube <- cross.validation(clusters.subtypes.all.tubes[[tube]], 
                                  subtype.len, requested.aml.type, 
                                  unmatch.penalty = sqrt(6), use.prop=T)
  cross.validation.scores[tube, ] <- scores.tube
  cat('\n')
}

# save the data
save(cross.validation.scores,
     file=paste(saved.data.path, "cross.validation.scores.RData", sep=""))

# just for test:
write.csv(t(cross.validation.scores[1,]), file = "~/Desktop/first-tube.csv")
          
# TODO : Check if it is working correctly
write.csv(cross.validation.scores, 
          file=paste(saved.data.path, "cross-validation-all.csv"))
## ----------------------------------------------------------------------------
## Compute statistics (Precision/Recall etc) from cross validation scores
## ----------------------------------------------------------------------------

load(file=paste(saved.data.path, "cross.validation.scores.RData", sep=""))
## Ariful ? we should remove rows with 0 or invalid entries ??
# pass subtype.len to it
cv.stats.all <- compute.stats(cross.validation.scores, subtype.indices)

# TODO : save the output.
# mean stats
cv.stats.mean = compute.stats.tube(colMeans(cross.validation.scores), aml.idx , healthy.idx)
# TODO : save the output
