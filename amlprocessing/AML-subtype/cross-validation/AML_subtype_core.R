classification.score <- function(sample, i , requested.subtype,
                                 other.subtypes, unmatch.penalty=999999, 
                                 use.prop=T, dist.type='Mahalanobis') { 
  # compute the classification.score for each sample vs provided 
  # templates
  #
  # Args:
  #   sample: TODO
  #   i: TODO
  #   requested.subtype:
  #   other.subtypes
  #   unmatch.penalty
  #   use.prop
  #   dist.type
  # Returns:
  #  TODO : fix it
  ################ debug_begin
  # TODO : remember to remove the debug-output.txt is saved from previous run   
  sink(paste(saved.data.path, "debug-output.txt", sep=""))
  cat('\n')
  cat("size of the sample is", sample@size, "\n")
  cat('the sample is number:', i)
  cat('\n')
  ################ debug_end
  
  # match a pair of templates using the mixed edge cover        
  mec <- match.clusters(requested.subtype, other.subtypes, 
                        unmatch.penalty=unmatch.penalty)
  # find the meta clusters that are specific to requested aml subtype
  unmatched.mc.req.subtype <- mcDetails(sample, unmatched=TRUE, matched=FALSE, 
                                        mec, c.size=FALSE)
  cat("mc that represent immunophenotype of requested subtype: ", "\n")
  debugMcDetails(unmatched.mc.req.subtype)
  
  ################ debug_begin
  
  # find the clusters that are matched and unmatched 
  # between sample and requested.subtype
  mec <- match.clusters(sample, requested.subtype, 
                        unmatch.penalty=unmatch.penalty)
  cat("length of match12 of sample vs requested subtype is", "\n")
  cat(length(mec@match12))
  cat("\n")
  mc.req.subtype <- mcDetails(sample, unmatched=TRUE, matched=TRUE,
                              mec, c.size=FALSE)  
  cat("unmatched mc between sample and requested subtype: ", "\n")
  debugMcDetails(mc.req.subtype[[1]])
  cat("matched mates between sample and requested subtype: ", "\n")
  debugMcDetails(mc.req.subtype[[2]])
  # print the |ci| of sample
  cat("size of each cluster", "\n")
  c.size.req <- mcDetails(sample, unmatched=TRUE, matched=TRUE,
                          mec, c.size=TRUE)
  debugMcDetails(c.size.req)
  
  # find the clusters that are matched and unmatched 
  # between sample and other subtypes
  mec <- match.clusters(sample, other.subtypes, 
                        unmatch.penalty=unmatch.penalty)
  cat("length of match12 of sample vs other subtype is", "\n")
  cat(length(mec@match12))
  cat("\n")
  mc.other.subtype <- mcDetails(sample, unmatched=TRUE, matched=TRUE,
                                mec, c.size=FALSE)
  cat("unmatched mc between sample and others: ", "\n")
  debugMcDetails(mc.other.subtype[[1]])
  cat("matched mates between sample and others: ", "\n")
  debugMcDetails(mc.other.subtype[[2]])
  # print the |ci| of sample
  cat("size of each cluster", "\n")
  c.size.other <- mcDetails(sample, unmatched=TRUE, matched=TRUE,
                            mec, c.size=TRUE)
  debugMcDetails(c.size.other)
  
  ################ debug_end
  
  score.requested <- semimatch.aml.template(sample, requested.subtype,
                                            unmatch.penalty=unmatch.penalty, 
                                            AML.mc=unmatched.mc.req.subtype, 
                                            use.prop=TRUE, 
                                            dist.type='Mahalanobis')
  
  score.others    <- semimatch.healthy.template(sample, other.subtypes,
                                                unmatch.penalty=unmatch.penalty,
                                                use.prop=TRUE, 
                                                dist.type='Mahalanobis')
  
  # the ones with requested subtypes have positive score
  # score <- ((score.requested) - (score.others))  
  score <- ((score.requested) + (score.others)) /2 
  # just for test
  score.details <- c(score, score.requested, score.others, score)
  #return (score)
  score.debug <- c(score.requested, score.others,
                   (score.requested + score.others)/2, 
                   ((score.requested) - (score.others))) 
  
  ################ debug_begin
  cat("\n")
  cat("score.req, score.other, avg, score.req - score.other")
  cat("\n")
  debugMcDetails(score.debug)
  cat('\n')
  sink()
  ################ debug_end
  
  return (score.details)
}


semimatch.template.subtype <- function(sample, template, AML.mc=NULL,
                                       unmatch.penalty=999999, use.prop=TRUE,
                                       dist.type='Mahalanobis'){
  ##########################################################################
  #
  # copy from Ariful semimatch.apl.template and 
  # change the name to semimatch.template.subtype
  ##########################################################################
  # browser()
  # would it be a problem if dM is calculated seperately?
  #  would this effect the result?
  dM = dist.matrix(sample, template, dist.type=dist.type)
  
  # have no clue why Ariful use this instead of match.clusters
  #   dM = dist.matrix(sample, requested.subtype, dist.type=dist.type)
  #   mec1 = computeMEC(dM, unmatch.penalty)
  #   mec = ClusterMatch(match12=mec1$match12, match21=mec1$match21,
  #   matching.cost=mec1$matching.cost, unmatch.penalty=unmatch.penalty)
  
  #   mec1 = computeMEC(dM, unmatch.penalty)
  #   mec = ClusterMatch(match12=mec1$match12, match21=mec1$match21, 
  #   matching.cost=mec1$matching.cost, unmatch.penalty=unmatch.penalty)
  mec <- match.clusters(sample, template, 
                        unmatch.penalty=unmatch.penalty)
  
  num.clusters = get.num.clusters(sample)
  semi.matching.cost = 0
  for(i in 1:length(mec@match12))
  {
    mates = mec@match12[[i]]
    d = 0
    frac = 1
    if(use.prop)
      frac = sample@clusters[[i]]@size/sample@size
    
    if(length(mates) > 0)
    {
      for(j in 1:length(mates))
      {
        d = d + dM[i,mates[j]]
      }
      
      d = d / length(mates)
      if(!is.null(AML.mc) && length(which(mates%in%AML.mc)) !=0)
      {
        #print("entering the if")
        d = (2*unmatch.penalty-d) * frac
        
      }
      else{
        #print("not meeting the if condition")
        d = - (1/num.clusters) * frac * (2*unmatch.penalty-d)
      }
    }
    
    semi.matching.cost = semi.matching.cost + d
  }
  
  return(semi.matching.cost)
}


compute.stats.tube <- function(scores.per.tube, req.subtype.indices, 
                               indices.of.others) {    
  ##############################################################################
  #FIXME : THIS SHOULD BE EDITED: ARIFUL DOCs
  ## -----------------------------------------------------------------------------
  ## This funciton computes statistics from the cross validation scores for a single tube
  ## (Precision', 'NPV', 'Recall/Sensitivity', 'Specificity', 'Accuracy', 'F-value)
  ## Input:
  ##      scores: a vector of size num.samples.per.tube. 
  ##      Each entry stores the cross-validation score for the sample.
  ##      normal.idx: indices of the healthy samples in clustSamples
  ##      aml.idx: indices of the AML samples in clustSamples
  ## Output:
  ##      statistics for each tube
  ## -----------------------------------------------------------------------------
  TP  <- length(which(scores.per.tube[req.subtype.indices]  > 0)) 
  FP  <- length(which(scores.per.tube[indices.of.others ]   > 0))
  TN  <- length(which(scores.per.tube[indices.of.others ]  <= 0))
  FN  <- length(which(scores.per.tube[req.subtype.indices] <= 0))   
  
  # TODO : fix it ; copy from Ariful's code
  # compute precision 
  PPV = TP/(TP+FP) 
  cat('ppv is :', PPV,'\n')
  
  NPV = TN/(TN+FN)
  cat('NPV is', NPV,'\n')
  cat(TN, FN)
  # compute recall
  Sensitivity = TP/(TP+FN) 
  Specificity = TN/(TN+FP)
  cat('Specificity is', Specificity, '\n')
  cat(TN, FP)
  Acc = (TP+TN) / (TP+FP+TN+FN)
  F1 = 2*PPV*Sensitivity / (PPV+Sensitivity)
  
  # TODO: fix this to just return what user had asked in requested.stat in top 
  # section of AML_subtype.R
  subtype.stat <- c(PPV, NPV, Sensitivity, Specificity, Acc, F1)
  return (subtype.stat)
}  

mcDetails <- function(sample, unmatched=FALSE, matched=FALSE, mec,
                      c.size=FALSE) {
  # TODO : print the details for each mc
  #
  # Args:
  #   sample: the cluster sample per object
  #       is the subtype_template and second one is template_other)
  #   unmatched:
  #   matched: 
  #   c.size :
  # Returns:
  #   redirect the output on sink and save in the folder 
  if(unmatched && ! matched) {
    # keep the size of each cluster
    cluster.subtype.size <- NULL
    unmatched.mc.req.subtype <- NULL
    for(i in 1:length(mec@match12)) {
      mates <- mec@match12[[i]]
      # browser()
      # cluster.subtype.size <- c(cluster.subtype.size, sample@clusters[[i]]@size)
      if(length(mates)==0) {
        unmatched.mc.req.subtype <- c(unmatched.mc.req.subtype, i)
      }
    }
    if (c.size) {
      return (cluster.subtype.size)
    } else {
      return (unmatched.mc.req.subtype)
    }     
  }
  
  return.mc <- list()  
  cluster.size <- NULL
  if(unmatched && matched) {
    unmatched.mc.sample <- NULL
    matched.mc.sample   <- NULL
    for(j in 1:length(mec@match12)) {
      mates <- mec@match12[[j]]
      cluster.size <- c(cluster.size, sample@clusters[[j]]@size)
      if(length(mates)==0) {
        unmatched.mc.sample <- c(unmatched.mc.sample, j)
      }
      # keep clusters that it has matched to
      if(length(mates)!=0) {
        matched.mc.sample   <- c(matched.mc.sample, mates)
      }      
    }  
    if (c.size) {
      return (cluster.size)
    } else {
      return.mc[[1]] <- unmatched.mc.sample
      return.mc[[2]] <- matched.mc.sample
      return (return.mc)    
    }    
  } 
}


debugMcDetails <- function(v.be.printed){  
  for(i in 1:length(v.be.printed)){
    cat(v.be.printed[[i]],' ')
  }  
  cat("\n")
}



sampleInUnmatchClusters <- function(mec, subtype.template) {
  # return the mc specific to the subtype along with the number of samples in
  # that cluster
  #
  # Args:
  #   mec: the matching between template of subtype of request and template of
  #      other subtype
  #   subtype.template : the subtype of interest in which we are identifying the
  #   mc specific for it  
  # Returns:
  #  a matrix in which its first column is the index of unmatched mc
  #  and its second column is the number of samples in that metacluster.
  #browser()
  samples.in.unmatch.mc <- matrix(0, nrow=0, ncol=2)
  for(i in 1:length(mec@match12)) {
    mates <- mec@match12[[i]]
    if(length(mates)==0){
      mc <- subtype.template@metaClusters[[i]]
      samples.in.unmatch.mc <- rbind(samples.in.unmatch.mc, 
                                     c(i, length(get.sample.indices(mc))))  
    }
  }
  return(samples.in.unmatch.mc)
}

sortMetaCluster <- function(mc.matrix, index, dec) {
  # return the mc with specific attributes.
  # TODO : more details.
  # TODO : add the mc.s and mc.end as input
  #
  # Args:
  #   mc. matrix : a matrix in which its first column is the meta cluster index
  #   and second column is the number of samples per that meta cluster.
  #   dec : we want to sort it based on the ascending or descending order
  #   mc.s: mc.end : range of highest number of meta cluster
  chosen.mc <- order(mc.matrix[,index], decreasing=dec)[mc.s:mc.end]
  return (chosen.mc)
}

subtypeIndex <- function(subtype.len, subytpe) {
  # get the start and end indices for each subtype
  # TODO : write doc
  # TODO : replace this part of the code with this func in all over the code
  subtype.v <- unlist(subtype.len)
  start <- sum(subtype.v [0:(subytpe-1)]) + 1
  end   <- sum(subtype.v [1:(subytpe)])
  indices <- c(start: end)
  return(indices)
}

mcDetailsPrint (index.chosen.mc, subtype.temp, samples.in.unmatched,
                req.subtype, cluster.per.tube)     

mcDetailsPrint <- function(chosen.mc, subtype.template, samples.in.unmatched,
                           k, clusters.per.tube) {
  cat("index of clusters in samples.in.unmatched", '\n')
  cat(chosen.mc)
  subtype.index <- subtypeIndex(subtype.len, k)
  #browser()
  for (i in 1: length(chosen.mc)) {
    # TODO Fix the naming
    #browser()
    mc.per.subtype  <- get.metaClusters(subtype.template)
    extracted.mc <- samples.in.unmatched[chosen.mc[i], 1]
    mc <- mc.per.subtype[[extracted.mc]]
    
    # use Ariful method
    cell.frac <- mc.stats (mc, clusters.per.tube, subtype.index)
    
    clusters <- mc@clusters
    cat('\n')
    title <- paste("[Tube ", tube.list[tube], 
                   "] #samples:", samples.in.unmatched[chosen.mc[i],2],
                   "  %cells:",  sprintf("%.2f", cell.frac[1]), 
                   " (sd ", sprintf("%.2f", cell.frac[2]), ")",sep="")
    cat(title, '\n')
  }
}











