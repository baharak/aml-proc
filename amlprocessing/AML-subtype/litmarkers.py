# extract the markers which literature had mentioned to be important for
# diagnosis of each subclass

import contextlib
import os.path
import sqlite3
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix
from sklearn.metrics import cohen_kappa_score
from statsmodels.stats.inter_rater import cohens_kappa, to_table
import math
import csv
from sklearn.metrics import precision_recall_fscore_support
from tabulate import tabulate

@contextlib.contextmanager
def sqlite_connection():
    db_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "all-subtypes-probs.sqlite")
    with sqlite3.connect(db_path) as conn:
        yield conn
    print "closing conn to db"

def tm(subtype):
    ## print the tube and marker
    ## based on markers in each tube
    ## and subclasses
    with sqlite_connection() as db:
        query = "select distinct tube, tubes.marker from tubes where marker in (SELECT marker FROM markerslit where sub="+subtype+") group by tubes.marker order by tube"
        print query
        tube, marker = zip(*list(db.execute(query)))
        table = [[0] for i in range(2)]
        table[0] = [t.encode() for t in tube]
        table[1] = [m.encode() for m in marker]
        print tabulate(table, headers=["tube", "marker", "subtype"],
                       tablefmt="latex")


if __name__=='__main__':
    # sub_name = ["AML with Monocytic Differentiation",
    #             "AML with Granulocytic Differentiation",
    #             "AML without Maturation"]
    ## markers listed in literature as important for each subclass
    tm("'md'") ## CD36 is not in my table
    print "for gd************"
    tm("'gd'") ## CD79 is not listed in my table
    print "for nm************"
    tm("'nm'")
