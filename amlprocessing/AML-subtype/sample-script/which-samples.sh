#!/bin/bash
scriptdir="$(dirname "$(readlink -f "$0")")"
egrep "\|$1(\$|\|)" "$scriptdir/samples-ok" | sed -r 's/(.{11}).*/\1/'
