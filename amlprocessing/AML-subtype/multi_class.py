import csv
import numpy as np
# from sklearn.svm import NuSVC
from sklearn.datasets import make_multilabel_classification
from sklearn.multiclass import OneVsRestClassifier
from sklearn.multiclass import OutputCodeClassifier
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
from sklearn import svm, grid_search

if __name__=='__main__':
    ## pure mean of the samples
    # path = "subtype-output/pure-data/binary/F1.txt"
    # csv = np.genfromtxt(path, delimiter=",")
    # x = csv[1:90, 1:7]
    # y = np.array(csv[1:90, 0])
    # print 'x is ', type(x), np.shape(x)
    # print 'y is ', type(y), np.shape(y)
    # classif = OneVsRestClassifier(SVC(kernel='rbf'))
    # classif.fit(x, y)
    # print classif.predict(np.array(csv[90:93, 1:7]))
    # print type(np.array(csv[90:93, 1:7])), np.shape(np.array(csv[90:93, 1:7]))

    ## reading the prob of svm result
    pos = [] ## positive samples
    file_names = ["md.csv", "gd.csv", "nm.csv"]
    label = 0
    for file in file_names:
        path = "subtype-output/" + file
        print "the file name is", path
        m_csv = np.genfromtxt(path, delimiter=",")
        nrow = np.shape(m_csv)[0]
        for row in range(1, nrow):
            if int(m_csv[row, 0]) == 2:
                m_csv[row, 0] = label
                # print m_csv[row, :]
                pos.append(m_csv[row,:])
        label = label + 1

    pos_r = len(pos)
    pos_c = len(pos[0])
    for i in range(0, pos_r):
        print "i is", i
        merge = pos[0:i] + pos[i + 1:]
        labels = [m[0] for m in merge]
        train = [m[1:] for m in merge]
        #print train
        i_ele = pos[i]
        test = i_ele[1:]
        training = np.array(train)
        testing = np.reshape(test, (1,2))
        labeling = np.array(labels)
        # print training * 1000
        # print type(training),  type(labeling), type(testing)
        # print np.shape(training),  np.shape(labeling),
        # np.shape(testing)

        # classif = OneVsRestClassifier(SVC(kernel='rbf'))
        # classif.fit(training * 100, labeling)
        # print (classif.predict(testing))

        params =  {'estimator': [
            SVC(kernel='rbf', C=0.1, gamma=0.0001),
            SVC(kernel='rbf', C=0.01, gamma=0.0001),
            SVC(kernel='rbf', C=0.001, gamma=0.0001),
            SVC(kernel='rbf', C=0.1, gamma=0.001),
            SVC(kernel='rbf', C=0.1, gamma=0.01),
            SVC(kernel='rbf', C=0.1, gamma=0.1),
            SVC(kernel='rbf', C=1, gamma=0.003),
            SVC(kernel='rbf', C=2, gamma=0.004),
            SVC(kernel='rbf', C=3, gamma=0.005),
            SVC(kernel='rbf', C=4, gamma=0.0003),
            SVC(kernel='rbf', C=2, gamma=0.00390625)
        ]}

        param_dist = {'C': pow(2.0, np.arange(-10, 11)),
                      'gamma': pow(2.0, np.arange(-10, 11)),
                      'kernel': ['linear', 'rbf']}
        #model = OneVsRestClassifier(params)
        # machine = grid_search.GridSearchCV(estimator=OneVsRestClassifier(param_dist),
        #                                    param_grid=params,
        #                                    n_jobs=-1)
        model_one = OneVsRestClassifier(params['estimator'])
        model_two = OneVsRestClassifier(param_dist)
        machine = grid_search.GridSearchCV(estimator=model_one,
                                           param_grid=params,
                                           n_jobs=-1)
#        model = OneVsRestClassifier(machine)
        print "type of machine is", machine
        machine.fit(training, labeling)
        print machine.predict(testing)



        # clf = svm.SVC()
        # clf.fit(train, map(int, labels))
        # print (clf.predict([test]))

        # clf = svm.SVC(decision_function_shape='ovo')
        # clf.fit(train, map(int, labels))
        # print (clf.predict([test])), i_ele[0]

        # clf = OutputCodeClassifier(LinearSVC(random_state=0),
        #                            code_size=2, random_state=0)
        # print type(np.array([train])), type(test)
        # print clf.fit(np.array(train), labels).predict(np.array([test]))
