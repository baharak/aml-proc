import sqlite3
import os.path
import contextlib
import matplotlib.pyplot as plt
plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import pandas


@contextlib.contextmanager
def sqlite_connection():
    db_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "all-subtypes-probs.sqlite")
    print db_path
    with sqlite3.connect(db_path) as conn:
        print "connect to db"
        yield conn
    print "closing conn to db"


def create_plot(table, title, ind, name):
    print table, title, ind, name
    with sqlite_connection() as db:
        tube, f1 =  zip(*list(db.execute(
            "select tube, case when f1 is null then 0 else f1 end from "
            + table +
            " where subind=?"
            " and tube not in ('all-tubes', 'F8') "
            " order by f1", (
            ind,))))

        y_pos = np.arange(len(tube))
        if (ind == 2):
            print tube, f1
        plt.barh(y_pos, f1, align='center', alpha=0.4)
        plt.yticks(y_pos, tube)
        plt.xlabel('F-val')
        plt.title('Diagnosis of AML '+ name + ' ' + title)
        plt.savefig("subtype-output/"+ table + "" + str(ind) + ".pdf")
        plt.clf()
        plt.close()


if __name__=="__main__":
    names = ["with Monocytic Differentiation",
             "with Granulocytic Differentiation",
             "Without Maturation"]
    tables = ["pure", "oldscore"]
    titles = ["based on raw data",
              "considering only similarity term"]
    ## 2d array, rows are subtype, columns are: pure and oldscore
    all_tubes = [[0 for x in range(len(tables))] for x in range(len(names))]
    for j in range(0, len(tables)):
        for i in range(0, len(names)):
            create_plot(tables[j], titles[j], i + 1, names[i])

            with sqlite_connection() as db:
                tube, f1 =  zip(*list(db.execute(
                    "select tube, case when f1 is null then 0 else f1 end from "
                    + tables[j] +
                    " where subind=?"
                    " and tube = 'all-tubes'"
                    " order by f1", (
                        (i+1),))))
                all_tubes[i][j] = list(f1)


    new_scoring = [0.98, 1, 1]
    pooling = [0, 0.2, 0.04]
    k = 0
    all_scores = []
    for row, col in all_tubes:
        all_scores.append([pooling[k]] + row + col + [new_scoring[k]])
        k += 1

    #labels = ["pooling", "pure", "oldscoring", "newscoring"]

    labels = ["a", "b", "c", "d"]
    scores = zip(*all_scores)
    # xxx change the title of the plot manually
    sub_indx = 1  # from 0-2 for each subtype
    y_pos = np.arange(len(all_scores[sub_indx]))
    width = 0.02
    plt.barh(y_pos, all_scores[sub_indx], align='center', alpha=0.4, color='r')
    plt.yticks(y_pos, labels)
    plt.xlabel('F-val')
    plt.title('Diagnosis of AML with Granulocytic Differentiation by combining all tubes')
    plt.savefig("subtype-output/"+ "all-tubes-gd" + ".pdf")
    plt.clf()
    plt.close()
