## re-print specific metaclusters in
## (marker, expression) level format
import sys
import re
import os
from os import walk
if __name__=='__main__':
    ## input: the directory to run the code
    ## output: append all the specific metaclusters in pairwise format
    ## cd MONOCYTIC DIFFERENTAION directory
    ## python ../../parsing.py >> selected
    fsep = '-'
    for file in next(os.walk('.'))[2]:
        file_name = file.split(fsep)
        if file_name[0] == 'patho':
            with open(file) as f:
                first_line = f.readline().strip()
                sep = ' |,'
                if first_line:
                    fline = re.split(sep, first_line)
                    for line in f.readlines():
                        line = line.strip()
                        if line:
                            mylist = re.split(sep, line)
                            ## pair everything except first, second and last three
                            shift = 1
                            value = [file_name[1]] + [
                                '(' + fline[i + shift] + ',' + ("%.2f" % float(mylist[i + shift])) + ')'
                                for i in range(1, 7)
                            ] + [mylist[-3], mylist[-2], mylist[-1]]
                            print >> sys.stdout, '\t'.join(value)
