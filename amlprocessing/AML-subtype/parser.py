import sys
import io
from collections import defaultdict

if __name__=='__main__':

    def readCdf(f):
        ## read cumulative distribution fun
        ## ./output/expression-output/final-interpret.csv
        hashtable = {}
        f.readline() # get rid of header
        print (f.readline)
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            word = line.split(',')
            if max(map(len, word)) == 0:
                break
            i = 2 # start of fs
            while i < len(word):
                pair = (word[0], word[i])
                if len(word[0]) != 0:
                    hashtable[pair] = float(word[i + 2])
                i = i + 3
        return hashtable


    def specificMC(f):
        ## parse specific metacluster file in
        ## ./AML-subtype/subtype-output/MONOCYTIC-DIFFERENTIATION/selected/
        ## these files have no header
        d = {}
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            word = line.split('\t')
            for w in word[1:]:
                tokens = w.strip('()').split(',')
                if len(tokens) == 2:
                    p = (word[0], tokens[0])
                    v = float(tokens[1])
                    d.setdefault(p, []).append(v)
        return d


    f = open(sys.argv[1], 'rb')
    hashtable = readCdf(f)
    g = open(sys.argv[2])
    d = specificMC(g)


    d_res = {}
    for common_key in set(d.keys()) & set(hashtable.keys()):
        vs = d[common_key]
        v2 = hashtable[common_key]
        exps = [
            None if abs(diff) < 1 else (False if diff < 0 else True)
            for diff in map(lambda x: x - v2, vs)
        ]
        # setdefault returns default={} or old
        d_sub = d_res.setdefault(common_key[0], {})
        for exp in exps:
            d_sub.setdefault(common_key[1], []).append(exp)

    print d_res
