import numpy as np
from statsmodels.stats.inter_rater import cohens_kappa, to_table
import sqlite3


def compute_f1(tp, fp, tn, fn):
    nom = (2 * tp)
    denom = (2 * tp + fp + fn)
    print nom, denom
    return (nom / float(denom))

def compute_kappa(a, b, c, d):
    # compute kapp for 2D confusion matrix
    # a: tp, b: fn, c: fp, d: tn
    n = a + b + c + d
    obs = (a + d) / n
    term_one = ((a + b) * (a + c)) / n
    term_two = ((c + d) * (b + d)) / n
    exp = (term_one + term_two) / n
    return (obs - exp) / (1 - exp)

if __name__=='__main__':
    ## compute cohens' kappa based on data of binary classifier
    print "gd_fp", cohens_kappa(np.array([[0, 16], [84 - 78, 78]]))
    print "md_fp", cohens_kappa(np.array([[11, 20],[70 - 61, 61]]))
    print "nm_fp", cohens_kappa(np.array([[2, 10], [88 - 84, 84]]))

    print "gd_fm", cohens_kappa(np.array([[16, 0], [0, 84]]))
    print "md_fm", cohens_kappa(np.array([[30, 1], [0, 69]]))
    print "nm_fm", cohens_kappa(np.array([[12, 0], [0, 88]]))
