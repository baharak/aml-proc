from collections import defaultdict
import sqlite3
import os.path

if __name__=='__main__':
    dic = defaultdict(list)
    with open('AML_tubes.csv') as tube:
        next(tube)
        for line in tube:
            line = line.rstrip().split(',')
            tube = line[0]
            markers = line[1].split('/')
            if tube == 'Iso' or tube =='AUTO' or tube =='LD':
                continue
            for marker in markers:
                dic[marker].append(tube)
            dic['FS'].append(tube)
            dic['SS'].append(tube)
        print dic

    db_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "AML-subtype/all-subtypes-probs.sqlite")
    with sqlite3.connect(db_path) as db:
        db.execute('create table if not exists markers(marker, tube, primary key(marker, tube))')
        for key, value in dic.iteritems():
            for val in value:
                print key, val
                db.execute('insert or ignore into markers values (?,?)', (key, val))
#     db_path = os.path.join(
#         os.path.dirname(os.path.realpath(__file__)),
#         "AML-subtype/all-subtypes-probs.sqlite")
#     print db_path
#     with sqlite3.connect(db_path) as db:
#         db.execute('create table if not exists tubes(marker, tube, primary key (marker, tube))')
#         print "connect to db"
#         for key, value in dic.iteritems():
# #            print "%s,%s" %(key, ','.join(value))
#             if len(value) != 4:
#                 print key, value
#             for val in value:
#                 db.execute('insert or ignore into tubes values (?,?)', (key, val))
#         db.exectute('insert or ignore into tubes values (?, ?)', ('FS', 'SS'))
