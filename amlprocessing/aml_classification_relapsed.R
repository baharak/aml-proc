
## -----------------------------------------------------------------------------
## This funciton compute a semi-matching from a sample to the healthy template
## Input:
##      clustSample: an object of ClusteredSample class representing 
##                the samples whose classification is to be computed 
##      template: the healthy template
##      unmatch.penalty= unmatch.penalty when matching clusters/metaclusters
##      use.prop = whether to use cell proportion when computing semi-match cost. I always set it to TRUE
##      dist.type= the method used to compute distance between clusters. Default  'Mahalanobis'
## returns the cost of semimatching with healthy template (see the AML paper)
## -----------------------------------------------------------------------------

semimatch.healthy.template <- function(clustSample, template, unmatch.penalty=999999,  use.prop=TRUE, dist.type='Mahalanobis')
{
  dM = dist.matrix(clustSample, template, dist.type=dist.type)
  mec1 = computeMEC(dM, unmatch.penalty)
  mec = ClusterMatch(match12=mec1$match12, match21=mec1$match21, matching.cost=mec1$matching.cost, unmatch.penalty=unmatch.penalty)
  num.clusters = get.num.clusters(clustSample)
  semi.matching.cost = 0
  for(i in 1:length(mec@match12))
  {    
    mates = mec@match12[[i]]
    d = 0
    frac = 1
    if(use.prop)
      frac = clustSample@clusters[[i]]@size/clustSample@size
    if(length(mates) > 0)
    {
      for(j in 1:length(mates))
      {
        d = d + dM[i,mates[j]]
      }
      d = d / length(mates)
      d = -(1/num.clusters) * frac *  (2*unmatch.penalty-d)
    }
    else
    {
      d = unmatch.penalty * frac
    }
    semi.matching.cost = semi.matching.cost + d     
  }
  return(semi.matching.cost)
}



## -----------------------------------------------------------------------------
## This funciton compute a semi-matching from a sample to the AML template
## Input:
##      clustSample: an object of ClusteredSample class representing 
##                the samples whose classification is to be computed 
##      template: the AML template
##      unmatch.penalty= unmatch.penalty when matching clusters/metaclusters
##      use.prop = whether to use cell proportion when computing semi-match cost. I always set it to TRUE
##      dist.type= the method used to compute distance between clusters. Default  'Mahalanobis'
## returns the cost of semimatching with AML template (see the AML paper)
## -----------------------------------------------------------------------------

semimatch.aml.template <- function(sample, template, AML.mc=NULL, unmatch.penalty=999999, use.prop=TRUE, dist.type='Mahalanobis')
{
  dM = dist.matrix(sample, template, dist.type=dist.type)
  mec1 = computeMEC(dM, unmatch.penalty)
  mec = ClusterMatch(match12=mec1$match12, match21=mec1$match21, matching.cost=mec1$matching.cost, unmatch.penalty=unmatch.penalty)
  num.clusters = get.num.clusters(sample)
  #unmatch.count = 0
  semi.matching.cost = 0
  for(i in 1:length(mec@match12))
  {    
    mates = mec@match12[[i]]
    d = 0
    frac = 1
    if(use.prop)
      frac = sample@clusters[[i]]@size/sample@size
    
    if(length(mates) > 0)
    {
      for(j in 1:length(mates))
      {
        d = d + dM[i,mates[j]]
      }
      
      d = d / length(mates)
      if(!is.null(AML.mc) && length(which(mates%in%AML.mc)) !=0)
      {
        d = (2*unmatch.penalty-d) * frac
        
      }
      else
        d = -(1/num.clusters) * frac * (2*unmatch.penalty-d)
    }
    #     else
    #     {
    #       #d = -(factor1) * frac * unmatch.penalty // does not work .... do not try
    #       unmatch.count = unmatch.count+1
    #     }
    semi.matching.cost = semi.matching.cost + d 
  }
  
  return(semi.matching.cost)
}




## -----------------------------------------------------------------------------
## This funciton compute a classification score of a sample
## Input:
##      clustSample: an object of ClusteredSample class representing 
##                the samples whose classification is to be computed 
##      template.healthy: the healthy template
##      template.aml: the AML template
##      unmatch.penalty= unmatch.penalty when matching clusters/metaclusters
##      use.prop = whether to use cell proportion when computing semi-match cost. I always set it to TRUE
##      dist.type= the method used to compute distance between clusters. Default  'Mahalanobis'
## Output:
##      returns a classification score (see the AML paper)
## -----------------------------------------------------------------------------

classification.score = function(clustSample,  template.healthy, template.aml, unmatch.penalty=999999, use.prop=T, dist.type='Mahalanobis')
{

  # keep the details of aml and healtht scores
  details.score <- NULL
  ## find the unmatched clusters in the aml template
  mec = match.clusters(template.healthy, template.aml, unmatch.penalty=unmatch.penalty)
  unmatched.mc.aml = NULL
  for(i in 1:length(mec@match21))
  {
    mates = mec@match21[[i]]
    if(length(mates)==0)
    {
      unmatched.mc.aml = c(unmatched.mc.aml, i)
    }
  }
  
  score.normal = semimatch.healthy.template(clustSample, template.healthy, unmatch.penalty=unmatch.penalty, use.prop=use.prop, dist.type=dist.type)
  score.aml = semimatch.aml.template(clustSample, template.aml, unmatch.penalty=unmatch.penalty, AML.mc=unmatched.mc.aml, use.prop=use.prop, dist.type=dist.type)
  score  = ( score.normal + score.aml) / 2
  details.score = c( details.score, score, score.normal, score.aml) 
  return (details.score)
}




## -----------------------------------------------------------------------------
## This funciton performs leave-one-out cross validation in the training set
## (We leave one sample out and create templates from the rest. The templates are
##  then used to classify the left-out sample.)
## Input:
##      clustSamples: a list ClusteredSample objects 
##      healthy.idx: indices of the healthy samples in clustSamples
##      aml.idx: indices of the AML samples in clustSamples
##      unmatch.penalty= unmatch.penalty when matching clusters/metaclusters
##      use.prop = whether to use cell proportion when computing semi-match cost. I always set it to TRUE
## Output:
##      returns a classification score for each sample in clustSamples
## -----------------------------------------------------------------------------

cross.validation = function(clustSamples, healthy.idx, aml.idx, unmatch.penalty = 9999999, use.prop=T)
{
  val.score = NULL
#  for(i in 1:length(clustSamples))
#   {
  for(i in c(2, 75)){
    cat(i, ' ')
    clustSample = clustSamples[[i]] # left out sample
    # create templates from rest of the samples
    template.aml = create.template(clustSamples[setdiff(aml.idx, i)], unmatch.penalty=unmatch.penalty)
    template.healthy = create.template(clustSamples[setdiff(healthy.idx, i)], unmatch.penalty=unmatch.penalty)
    save(template.aml, file=paste(source.c, "template.aml.relapsed.RData", sep=""))
    save(template.healthy, file=paste(source.c, "template.healthy.relapsed.RData", sep=""))
    score = classification.score(clustSample, template.healthy, template.aml, use.prop=use.prop, unmatch.penalty=unmatch.penalty)
    print(paste('size of score in cross.validation :', length(score) ))
    val.score = c( val.score, score)  
  }
#  }
  return(val.score) 
}


## -----------------------------------------------------------------------------
## This funciton computes statistics from the cross validation scores for all tubes
## (Precision', 'NPV', 'Recall/Sensitivity', 'Specificity', 'Accuracy', 'F-value)
## Input:
##      scores: a matrix of size num.tubes x num.samples.per.tube. 
##      Each entry stores the cross-validation score for the sample.
##      normal.idx: indices of the healthy samples in clustSamples
##      aml.idx: indices of the AML samples in clustSamples
## Output:
##      statistics for each tube
## -----------------------------------------------------------------------------

compute.stats = function(scores, aml.idx, normal.idx)
{
  numTubes = nrow(scores)
  stats = matrix(0, numTubes+2 ,6)
  colnames(stats) = c('Precision', 'NPV', 'Recall/Sensitivity', 'Specificity', 'Accuracy', 'F-value')
  for(tube in 1:numTubes)
  {
    stats[tube,] = compute.stats.tube(scores[tube,], aml.idx, normal.idx)
  }
  stats[numTubes+1,] = compute.stats.tube(colMeans(scores), aml.idx, normal.idx) 
  #stats[9,] = compute.stats.tube(colMeans(scores[4:6,]), aml.idx, normal.idx) 
  stats[numTubes+2,] = compute.stats.tube((scores[4,]+2*scores[5,]+3*scores[6,])/6, aml.idx, normal.idx) 
  return(stats)
}


## -----------------------------------------------------------------------------
## This funciton computes statistics from the cross validation scores for a single tube
## (Precision', 'NPV', 'Recall/Sensitivity', 'Specificity', 'Accuracy', 'F-value)
## Input:
##      scores: a vector of size num.samples.per.tube. 
##      Each entry stores the cross-validation score for the sample.
##      normal.idx: indices of the healthy samples in clustSamples
##      aml.idx: indices of the AML samples in clustSamples
## Output:
##      statistics for each tube
## -----------------------------------------------------------------------------

compute.stats.tube = function(scores, aml.idx , normal.idx)
{
  TP = length(which(scores[aml.idx]  > 0 )) # True positive
  FN = length(which(scores[aml.idx]  <= 0 )) # False negative
  TN = length(which(scores[normal.idx]  <= 0 )) # True positive
  FP = length(which(scores[normal.idx]  > 0 )) # False negative
  #TP = TP * ratio
  #FN = FN * ratio
  PPV = TP/(TP+FP) # precision
  NPV = TN/(TN+FN)
  Sensitivity = TP/(TP+FN) #recall
  Specificity = TN/(TN+FP)
  Acc = (TP+TN) / (TP+FP+TN+FN)
  F1 = 2*PPV*Sensitivity / (PPV+Sensitivity)
  return(c(PPV, NPV, Sensitivity, Specificity, Acc, F1))
}



# compute.stats.tube(scores.train[1,], aml.idx.train, normal.idx.train)
# val.results.all1 = val.results.all
# for(tube in 4:6)
# {
#   val.results.all1[tube, val.results.all[tube, ]>0 ] = val.results.all[tube, val.results.all[tube, ]>0] * which(order(SS[4:6,2])==(tube-3))
#   val.results.all1[tube, val.results.all[tube, ]<=0] = val.results.all[tube, val.results.all[tube, ]<=0] * which(order(SS[4:6,1])==(tube-3))
# }
# 
# val.results.all1 = val.results.all
# for(tube in 1:7)
# {
#   val.results.all1[tube, val.results.all[tube, ]>0 ] = val.results.all[tube, val.results.all[tube, ]>0] * which(order(SS[,2])==tube)
#   val.results.all1[tube, val.results.all[tube, ]<=0] = val.results.all[tube, val.results.all[tube, ]<=0] * which(order(SS[,1])==tube)
# }
# 



## -----------------------------------------------------------------------------
## This funciton plot the classification scores
## Input:
##      classification.score: a vector of size num.samples.per.tube. 
##          Each entry stores the cross-validation score for the sample.
##      normal.idx: indices of the healthy samples in clustSamples
##      aml.idx: indices of the AML samples in clustSamples
## Output:
##      plot
## -----------------------------------------------------------------------------

plot.classification=function(classification.score, aml.idx, normal.idx, title="")
{
  dist.normal = sort(classification.score[normal.idx])
  dist.aml = sort(classification.score[aml.idx])
  total = length(dist.normal) + length(dist.aml)
  plot(0, xlim = c(0,total), ylim=c(min(dist.normal), max(dist.aml)), type='n', xlab='Rank of test samples (ascending order of classification score)', 
       #ylab=expression('D(S, T'['healthy'] ~ ')' ~~ - ~ 'D(S, T'['aml'] ~ ')') )
       ylab='Classification score', main=title )
  #abline(h=38)
  points(dist.normal, col='blue', pch=1, cex=1, lwd=1.5)
  points((length(dist.normal)+1):total, dist.aml, col='red', pch=17)
  abline(h=0, col='black', lwd=2)
  abline(v=length(dist.normal), lwd=2)
  legend("topleft", inset=.05, title="Sample status      ", box.col = "white",
         c("Healthy   ","AML"), pch=c(1,17), col=c('blue', 'red'), cex=1, pt.lwd=2, pt.cex=1.3, horiz=TRUE)
  
}
