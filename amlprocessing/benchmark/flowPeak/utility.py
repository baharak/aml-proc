import os
from os import listdir
from os.path import isfile, join
#from os import * : replace path with path.join : but not encouraged

class input_file:
    def _input(self, path):
        just_files = [f for f in listdir(path)
                      if isfile(join(path, f))]
        full_path = [join(path, f) for f in just_files]
        return full_path
