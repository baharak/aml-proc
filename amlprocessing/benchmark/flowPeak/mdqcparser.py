## parse all of the files in output/mdqc-f-val/
## extract F-value and associated gate size
## for every subtype
## To run: python mdqcparser.py output/mdqc-f-val/

from utility import input_file
import sys
import re
import os
from collections import defaultdict
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

if __name__=='__main__':
    accum = []
    for f in input_file()._input(sys.argv[1]):
        m = re.search(r'^gate-([^-]*)-(.*)',
                      os.path.basename(f))
        if m is not None:
            with open(f, 'r') as stat:
                next(stat)
                for line in stat:
                    accum.append([m.group(1), # gate size
                                  m.group(2), # subclass
                                  line.split(',')[9], # F-val
                                  line.split(',')[0]]) # True pos

    dic = defaultdict(list)
    for line in accum:
        dic[line[1]].append([line[0],
                             0 if line[2] == 'NaN' else line[2]])

    ## plot every subtype
    plot_dic = defaultdict(list)
    for key , val in dic.iteritems():
         ## _ is for private method/field/function in a class or a hideen argument (and not for a local var)
        _l1 = []
        _l2 = []
        for v in val:
            _l1.append(v[0])
            _l2.append(v[1])
        plot_dic[key].append([_l1, _l2])
    print plot_dic
    vals = plot_dic.values()

    # plt.plot(vals[0][0][0], vals[0][0][1], 'r^',
    #          vals[1][0][0], vals[1][0][1], 'bs',
    #          vals[2][0][0], vals[2][0][1], 'g^')
    # replace this mess with:
    colors = ['r^', 'bs', 'g^']
    plot_args = () # can be any iterable obj, even a list
    for ind, color in enumerate(colors):
        plot_args += (vals[ind][0][0], vals[ind][0][1], color)
    plt.plot(*plot_args, label=['1','2','3'])
    plt.legend(['Granulocytic Diff', 'Monocytic Diff', 'Without maturation'],
               bbox_to_anchor=(.05, 1), loc=2, borderaxespad=0.)
    plt.xlabel('radius of gate')
    plt.ylabel('F-val')
    plt.savefig('radius-fval.pdf')
