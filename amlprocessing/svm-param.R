# How the svm performance of classification (F-value) changes 
# by tunning gamma and cost

for (var in c("gammas","costs")) {
	for (subtype in 1:length(requested.aml.type)) {  
		file.name    <- paste(requested.aml.type[[subtype]], var, sep="-")
		path.to.file <- paste(saved.tmp, file.name, sep="")    
		table <- read.csv(path.to.file, header=FALSE)
		variable <- table[[1]]
		f.value  <- table[[3]]
		non.nan  <- which(!is.nan(f.value))
		# the value of gammas and cost that F-value is nan for them.
		var.is.nan <- variable[which(is.nan(f.value))]
		cons <- table[2][[1]][[1]] 
		plot.title <- paste("const=", cons, "F-value is NAN for", 
				length(var.is.nan), "different", var)
		pdf(paste(saved.tmp, paste(file.name , "pdf", sep="."), sep='/'))    
		plot(variable[non.nan], f.value[non.nan], type="l", col="red",
				xlab=var, ylab="F-value", main=plot.title)
		dev.off()
	}
}

## TODO : dirty copy to run for flowPeaks, names of output's files of flowPeaks
## should change.
saved.tmp <- "/home/bsaberid/Desktop/repo/amlprocessing/output/flowPeaks/NM/"
setwd("/home/bsaberid/Desktop/repo/amlprocessing/output/flowPeaks/NM/")
for (var in c("gammas","costs")) {
	for (subtype in 1:length(requested.aml.type)) {  
		file.name    <- paste("all", var, sep="-")    
		path.to.file <- paste(saved.tmp, file.name, sep="")    
		table <- read.csv(path.to.file, header=FALSE)
		variable <- table[[1]]
		f.value  <- table[[3]]
		non.nan  <- which(!is.nan(f.value))
		# the value of gammas and cost that F-value is nan for them.
		var.is.nan <- variable[which(is.nan(f.value))]
		cons <- table[2][[1]][[1]] 
		plot.title <- paste("const=", cons, "F-value is NAN for", 
				length(var.is.nan), "different", var)
		pdf(paste(saved.tmp, paste(file.name , "pdf", sep="."), sep='/'))    
		plot(variable[non.nan], f.value[non.nan], type="l", col="red",
				xlab=var, ylab="F-value", main=plot.title)
		dev.off()
	}
}
