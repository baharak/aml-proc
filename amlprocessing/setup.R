library(flowCore)
library(flowMatch)
library(e1071) # for svm
library(fpc) # for dbscan
library(vegan) # for isomap
library(vegan3d) # for 3D plot of isomap
library(rgl)
library(compositions)

funcSource <- function(source.c, fun.name ) {
  fun.path <- paste(source.c, fun.name, sep='/')
  if(!exists(fun.name)) source(fun.path)
}

cat("\n***set source.c to directory of your code\n")
source.c <- paste(getwd(), '/', sep='')
cat("\nsource.c is :", source.c, "\n")
saved.data.path <- paste(source.c, "input/tubes-data/", sep='/')
transformed.data <- paste(source.c, "input/transformed-data", sep='/')
out.path <- paste(source.c, "output", sep='/')
output.path      <- paste(source.c, "output/ranking/", sep='/')
healthy.one <- list.files(paste(source.c, "input/DonorRawFiles/", sep= '/'))
patient.path <- paste(source.c, 'input/patient-excels/', sep='/')
source(paste(source.c, 'interface.R', sep='/'))
source(paste(source.c, 'core.R', sep='/'))
aml.start <- 101
all.len   <- 200
healthy.idx <- 1:100
aml.idx <- 101:200
files.list   <- list.files(patient.path)
all.indices  <- unlist(strsplit(files.list, ".xls"))
h.list <- as.numeric(unlist(healthy.one)) # TODO : also print the healthy indices
kclose <- 6
# the penalty for leaving a node without partner (leave a node unmatched)
penalty <- sqrt(6)
cell.portion <- TRUE
myeloid.tubes <- c('F9', 'F41', 'F42', 'F43', 'F106')
# for each experiment
default.subtype <- FALSE #T: necessary matrices & indices for subtype experiment(XXX:it is copy-pasted in flowPeaks part that can be removed)
default.benchmark <- FALSE
subtype.flowPeaks <- FALSE #T: running subtype experiment with flowPeaks
aml.healthy.flowPeaks <- FALSE #T: running aml vs healthy experiment with flowPeaks
default.flowmatch <- FALSE #T: create initial clusters with kmeans for flowpeaks
dbscan <- FALSE #T: create initial clusters with DBSCAN for flowpeaks
read.clusters <- FALSE  #T: saved the clusters of each sample in a matrix(it is a tube experiment)
aml.vs.healthy <- FALSE #T: running aml vs healthy experiment with flowMatch
ans1 <- NA
ans2 <- NA
while(is.na(ans1) || is.na(ans2)) {
  cat("\n*what experiemnt are you running?\n",
      "\n Note: just choose one of the following:\n")
  cat("\n-bp for flowPeaks benchmark\n",
      "\n-faml for flowMatch AML vs Healthy\n",
      "\n-fsubtype for flowMatch AML subtypes\n")

  ans1 <- readline()
  if (ans1=="fsubtype") {
    default.subtype <- TRUE
  } else if (ans1=="bp") {
    default.benchmark <- TRUE
  } else if (ans1=="faml") {
    default.flowmatch <- TRUE
  }
  cat("\n is this a test experiment? 'yes' or 'no'\n")
  ans2 <- readline()
  if (ans2=="yes") {
      tube.list <- "F9"
  } else {
      tube.list   <- c('F1', 'F2', 'F3',
                       'F4', 'F5', 'F7',
                       'F8', 'F9', 'F41',
                       'F42', 'F43', 'F106',
                       'Iso')
  }
}

cat("list of the tubes are:")
cat(tube.list)

if (default.flowmatch) {
    # for each tube, create cluster samples for each sample
    cat("\n*using kmeans clusters as initial clusters for flowMatch...\n")
    tubeCluster(saved.data.path,
                readCluster,
                "kmeans-clusters.rds",
                saved.data.path)
}

if (dbscan) {
    # change the just transformed data from flowPeaks to another dir
    transformed.data <- paste(source.c,
                              "benchmark/flowPeak/input/",
                              sep='/')
    tubeCluster(transformed.data,
                dbscanClusters,
                "dbscan-clusters.rds",
                saved.data.path)
}

if (read.clusters) {
   cat("computing tubes.clusters \n")
   # read saved cluster samples for each tube
   tubes.clusters <- list()
   for (i in 1:length(tube.list)) {
      tube <- tube.list[[i]]
      tube.path <- paste(saved.data.path, tube, sep='')
      to.read <- paste(tube.path, "kmeans-clusters.rds", sep='/')
      tubes.clusters[[i]] <- readRDS(to.read)
   }
}

## Setting default.flowMatch = TRUE and read.clusters = TRUE,
## this part can be run independently
if (aml.vs.healthy) {
    cat("running aml vs healthy \n")
    source("aml_classification.R")
    source("cross_validation.R")
}

if (default.subtype) {
   cat("\n*necessary setup for running subtype experiment...\n")
   source(paste(source.c, "AML-subtype/init.R", sep=''))
   setwd(source.c)
   subtype.predict <- FALSE
   cluster.sample  <- TRUE # should be FALSE for PCA experiment
   init(paste(source.c, "AML-subtype/", sep=''))
   my.scoring <- FALSE # use scoring function that I have
}

## This part can be run independently
if (default.benchmark) {
    cat("\n*running benchmarks for flowPeaks...\n")
    #install.packages("permute")
    #install.packages("data.table")
    library(data.table)
    library(permute)
    library(flowViz)
    #gsl should be installed (sudo apt-get install libgsl0-dev
    library(flowPeaks)
    library(MASS)
    benchmark.path <- paste(source.c, "/benchmark/flowPeak/", sep='')
    #XXX
    fp.o <- paste(benchmark.path, "output", sep="")
    dir.create(fp.o, showWarning = FALSE)
    funcSource(benchmark.path, "interface.R")

    if (aml.healthy.flowPeaks) {
        cat("\n--AML-vs-healthy--\n")
        hardcode.subtype <<- 1 # used in applySVM, XXX should be passed as input param
        ## run flowPeaks for AML vs healthy and
        ## apply svm on feature vectors
        subtype.name <<- NULL
        benchmarkPerTube(benchmarkAML, "aml-healthy")
    }

    if (subtype.flowPeaks) {
        ## init(): initialization of indices for AML subtype
        ## benchmarkSubtype(): prepare two sets of flowsets in which
        ## each flowframe is labeled as subtype or ~subtype
        ## benchmarkSubtype's output is passed to benchmarkPerTube()
        ## in this method, flowsets are mixed and flowPeak() is applied
        ## on each flow frame; vector are created for each flowframe

        cat("\n--for subtype--\n")
        ## XXX
        source(paste(source.c, "AML-subtype/init.R", sep=''))
        setwd(source.c)
        cluster.sample <- FALSE
        init(paste(source.c, "AML-subtype/", sep=''))

        ## XXX it should be running for each subtype and saving path
        ## must get updated to save the result for each subtype seperately
        for (sub.idx in 1:length(requested.aml.type)) {
            subtype.name <<- requested.aml.type[[sub.idx]]
            hardcode.subtype <<- sub.idx # need in accuracy function
            cat('for subtype ', subtype.name, sep='')
            cat('\n')

            #getDistancePerTube(function(tube) benchmarkSubtype(tube, sub.idx))

            for (dis in seq(20, 180, 20)) {
                cat('radius of the gate is ', dis, sep='')
                benchmarkPerTube(function(tube) benchmarkSubtype(tube, sub.idx),
                                 "subtype", dis)
                ## XXX
                ## combining the tubes for flowPeaks benchmark.
                combined.data <- combineTubes(subtype.name)
                stat <- applySVM (combined.data)
                ## XXX the output folder should be mixedflowset/subtype
                out.n <- paste(paste("gate", dis, sep="-"),
                               subtype.name, sep='-')
                sink(paste(fp.o, out.n, sep='/'))
                ## XXX
                cat("TP", "TN", "FN", "FP", sep=",")
                cat(",PPV", "NPV", "Sensitivity", "Specificity", "Acc", "F1",  sep=",")
                cat("\n")
                cat(stat, sep=',')
                sink()
                closeAllConnections()
                cat(paste("the time is", Sys.time()))
                cat("\n")
            }
        }
    }
}
